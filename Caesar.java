/**
 * Encoder and decoder for Caesar Cipher
 * @author Franciszek Wojciechowski
 */

public class Caesar extends MonoAlphaSubstitution {
    private int shift;
    private String text = "";
    /**
     * Process a alphabet order according to the deafult shift.
     * @return Returns regular alphabet string.
     */
    public Caesar(){
    }
    /**
     * Process a alphabet order according to the shift.
     * @param key A shift that will be made.
     * @return Encrypted or decrypted text from MonoAlphaSubstitution.
     */ 
    public Caesar(int key){ //int
        shift = key;
        char c;
        int g;
        String keyc = "";
        int x = 0;
        for(int z = 'a'; z <= 'z'; z++){
            int valueChar = z;
            if(key < 0){
                valueChar = valueChar - 'z';
            }else{
                valueChar = valueChar - 'a';
            }
            c = (char) z;
            keyc = keyc + c;
            if(key < 0){
                g = 'z' + ((valueChar + key) % 26);
            }else{
                g = 'a' + ((valueChar + key) % 26);
            }
            c = (char) g;
            keyc = keyc + c;
            x++;
        }
        MonoAlphaSubstitution demo = new MonoAlphaSubstitution(keyc);
        text = demo.mapping;
    }
    /**
     * Retrieves an input and return encrypted or decrypted text.
     * @param args Requires three parameters a integer for number of shifts, and a string/character which will be encrypted.
     */
    public static void main(String[] args){
        if(args.length == 3){
            if(args[0].equals("encrypt")){
                int mov = Integer.parseInt(args[1]);
                Caesar s = new Caesar(mov);
                System.out.println(s.encrypt(args[2]));

            }else if(args[0].equals("decrypt")){
                int mov = Integer.parseInt(args[1]);
                Caesar s = new Caesar(mov*-1);
                System.out.println(s.decrypt(args[2]));
            }else{
                System.out.println("The first parameter must be \"encrypt\" or \"decrypt\"!\nUsage: java Caesar encrypt n \"cipher text\"");
            }
        }else if(args.length > 3){
            System.out.println("Too many parameters!\nUsage: java Caesar encrypt n \"cipher text\"");
        }else if(args.length < 3){
            System.out.println("Too few parameters!\nUsage: java Caesar encrypt n \"cipher text\"\n");
        }
    }
    
}
