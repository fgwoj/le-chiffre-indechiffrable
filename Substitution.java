/**
 * Substitution class
 * @author Franciszek Wojciechowski
 */
public abstract class Substitution implements Cipher{
    /**
     * Gets a character which will be processed.
     * @param c A character that will be checked.
     * @return Processed character.
     */
    public abstract char encrypt(char c);
    public abstract char decrypt(char c);
    
    /**
     * Converts string to character and process it in another encrypt method
     * @param plaintext A string that will be processed. 
     * @return A string with encrypted text.
     */
    public String encrypt(String plaintext){
        char[] d = plaintext.toCharArray();
        String finalString = "";
        for(int i = 0; i < d.length; i++){
            if(d[i] < 'a'){
                finalString += Character.toUpperCase(encrypt(d[i]));

            }else{
                finalString += encrypt(d[i]);
            }
        }
        return finalString;
    }
    /**
     * Converts String to character and process it in another decrypt method
     * @param cryptotext A string that will be processed. 
     * @return A string with decrypted text.
     */
    public String decrypt(String cryptotext){
        char[] d = cryptotext.toCharArray();
        String finalString = "";
        for(int i = 0; i < d.length; i++){
                if(d[i] < 'a'){
                    finalString += Character.toUpperCase(decrypt(d[i]));

                }else{
                    finalString += decrypt(d[i]);
                }
            }
        return finalString;
    }
  
}