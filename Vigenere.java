/**
 * Encoder and decoder for Vigenere Cipher
 * @author Franciszek Wojciechowski
 */
public class Vigenere extends Substitution{
    private String ciphers = "";
    private int position = 0;
    /**
     * Required to terminate processing of user's input.
     * @return Returns a "key" with 0 which will terminate processing as it will use deafault alphabet.
     */
    public Vigenere(){
        ciphers = "0";
    } 
    /**
     * Converts a secret key to lowercase. 
     * @param key A string that will be converted.
     * @return Converted string which will be used for processing user's input.
     */
    public Vigenere(String key){
        String text = key.toLowerCase();
        char[] listComp = text.toCharArray();
        int[] intA = new int [listComp.length];

        for(int i = 0; i < listComp.length; i++){
            intA[i] = intA[i] + listComp[i];
            ciphers += (char) intA[i]; 
        }
    } 
    /**
     * Encrypts a character by using formula.
     * @param c The character that will be encrypted.
     * @return An encrypted character.
     */
    public char encrypt(char c){
        if(ciphers.equals("0")){
            return c;
        }
        if(Character.isAlphabetic(c)){
            int checker = c;
            String enc = ciphers;
            char [] de = enc.toCharArray();
            if (Character.isLowerCase(c)){
                checker = 'a' + (checker + de[position] - 2 * 'a') % 26;
                c = (char) checker;

            } else if (Character.isUpperCase(c)){
                checker = 'A' + (checker + de[position]  - 2 * 'A') % 26;
                c = (char) checker;
            }
        }
        char [] x = ciphers.toCharArray();
        position += 1;
        if (position >= x.length){
            position = 0;
        }
        return c;
    }  
    /**
     * Decrypts a character by using formula.
     * @param c The character that will be decrypted.
     * @return An decrypted character.
     */
    public char decrypt(char c){
        if(ciphers.equals("0")){
            return c;
        }
        if(Character.isAlphabetic(c)){
            int checker = c;
            String enc = ciphers;
            char [] de = enc.toCharArray();
            if (Character.isLowerCase(c)){
                checker = 'a' + (checker - de[position]  + 26) % 26;
                c = (char) checker;

            } else if (Character.isUpperCase(c)){
                checker = 'A' + (checker - de[position]  + 26) % 26;
                c = (char) checker;
            }
        }
        char [] x = ciphers.toCharArray();
        position += 1;
        if (position >= x.length){
            position = 0;
        }

        return c;
    }
    /**
     * Retrieves an input and return encrypted or decrypted text.
     * @param args Requires three parameters, an user's option, secret key, and a string/character which will be encrypted or decrypted.
     */
    public static void main(String[] args){
        if(args.length == 3){
            if(args[0].equals("encrypt")){
                Vigenere s = new Vigenere(args[1]);
                System.out.println(s.encrypt(args[2]));

            }else if(args[0].equals("decrypt")){
                Vigenere s = new Vigenere(args[1]);
                System.out.println(s.decrypt(args[2]));
                
            }else{
                System.out.println("The first parameter must be \"encrypt\" or \"decrypt\"!\nUsage: java Vigenere encrypt key \"cipher text\"");
            }
        }else if(args.length > 3){
            System.out.println("Too many parameters!\nUsage: java Vigenere encrypt key \"cipher text\"");
        }else if(args.length < 3){
            System.out.println("Too few parameters!\nUsage: java Vigenere encrypt key \"cipher text\"");
        }
    }

}
