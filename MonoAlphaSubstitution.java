/**
 * Encoder and decoder for Mono Alpha Substitution
 * @author Franciszek Wojciechowski
 */
public class MonoAlphaSubstitution extends Substitution{
    protected static String mapping = "";
    /**
     * Creates a regular order alphabet as a deafult. 
     * @return Proccesed alphabet that will be used for encryption or decryption.
     */
    public MonoAlphaSubstitution(){
        for(int i = 'a'; i <= 'z'; i++){
            mapping += (char) i;
        }
    }
    /**
     * Takes a secret key (user's alphabet) and adds every character to regular alphabetic order. 
     * @param key The secret key (user's alphabet)
     * @return Proccesed alphabet that will be used for encryption or decryption.
     */
    public MonoAlphaSubstitution(String key){
        String fS = "";
        mapping = key;
        char [] coverted = mapping.toCharArray();
        int check = coverted.length;
        int[] letters = new int[26];
        int[] capitalLetters = new int[26];
        char[] letterChar = new char[26]; // final table
        int x = 0;
        int v = 0;
        for(int g = 'A'; g <= 'Z'; g++){
            capitalLetters[x] = g;
            x++;
        }
        x = 0;
        for(int z = 'a'; z <= 'z'; z++){
            letters[x] = z;
            if(v < check){
                if (letters[x] == coverted[v]){
                    letters[x] = coverted[v+1];
                    letterChar[x] = (char) letters[x];
                    fS = fS + letterChar[x];
                    v += 2;
                    x++;
                }else{
                    letterChar[x] = (char) letters[x];
                    fS = fS + letterChar[x];
                    x++;
                }    
            }else{
                letterChar[x] = (char) letters[x];
                fS = fS + letterChar[x];
                x++;
            }
        }
        mapping = fS;
    }
    
    /**
     * Encrypts a character by using processed alphabet.
     * @param c The character that will be encrypted.
     * @return A encrypted character.
     */
    public char encrypt(char c){
        if(Character.isAlphabetic(c)){
            char[] table = mapping.toCharArray();
            int[] letters = new int[26];
            int x = 0;
            if(c < 'a'){
                for(int z = 'A'; z <= 'Z'; z++){
                    letters[x] = z;
                    x++;
                }
            }else{
                for(int z = 'a'; z <= 'z'; z++){
                    letters[x] = z;
                    x++;
                }
            }
            int i = 0;
            while(c != letters[i]){
                i++;
            }

            c = table[i];
            c = (char) c;
        }
        return c;
    }
    /**
     * Decrypts a character by using processed alphabet.
     * @param c The character that will be decrypted.
     * @return A decrypted character.
     */
    public char decrypt(char c){
        if(Character.isAlphabetic(c)){
            char[] table = mapping.toCharArray();
            int[] letters = new int[26];
            int x = 0;
            if(c < 'a'){
                for(int z = 'A'; z <= 'Z'; z++){
                    letters[x] = z;
                    x++;
                }
            }else{
                for(int z = 'a'; z <= 'z'; z++){
                    letters[x] = z;
                    x++;
                }
            }
            int i = 0;
            while(c != letters[i]){
                i++;
            }

            c = table[i];
            c = (char) c;
        }
        return c;
    }
    /**
     * Retrieves an input and return encrypted or decrypted text.
     * @param args Requires three parameters, an user's option, secret key, and a string/character which will be encrypted or decrypted.
     */
    public static void main(String[] args){
        if(args.length == 3){
            if(args[0].equals("encrypt")){
                MonoAlphaSubstitution m = new MonoAlphaSubstitution(args[1]);
                System.out.println(m.encrypt(args[2]));
            }else if(args[0].equals("decrypt")){
                MonoAlphaSubstitution m = new MonoAlphaSubstitution(args[1]);
                System.out.println(m.decrypt(args[2]));
            }else{
                System.out.println("The first parameter must be \"encrypt\" or \"decrypt\"!\nUsage: java MonoAlphaSubstitution encrypt key \"cipher text\"");
            }
        }else if(args.length > 3){
            System.out.println("Too many parameters!\nUsage: java MonoAlphaSubstitution encrypt key \"cipher text\"");
        }else if(args.length < 3){
            System.out.println("Too few parameters!\nUsage: java MonoAlphaSubstitution encrypt key \"cipher text\"");
        }
    }
}